//
//  AppDelegate.swift
//  Postcards Demo
//
//  Created by Will Gunby on 25/04/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//

import UIKit
import CoreData


func setBuilderState(showImage:Bool){
    NSUserDefaults.standardUserDefaults().setObject(showImage, forKey: "BuilderState.ShowImage")
    NSUserDefaults.standardUserDefaults().synchronize()
}
func getBuilderState()->Bool{    
    if let value = NSUserDefaults.standardUserDefaults().objectForKey("BuilderState.ShowImage") as? Bool {
        return value
    }
    return false
}


var player = MediaPlayer()

var appThemeColour = UIColor(netHex:0x57AF00)
var appThemeColourLight = UIColor(netHex:0xA4EE5B)
var networkTimeoutSecs:NSTimeInterval = 5

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, WebserviceResponse {

    var window: UIWindow?
    
    /* background support */
    var backgroundTaskId = UIBackgroundTaskInvalid
    var taskTimer : NSTimer?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        BuddyBuildSDK.setup()
        
        // Override point for customization after application launch.
        
        //set default window tint (effectively sets defailt text colour in a bunch of controls
        self.window!.tintColor = appThemeColour  //UIColor.whiteColor()
        
        //set the default text colour on all nav bar buttons
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().backgroundColor = appThemeColour
        
        setBuilderState(false)
        
        //BckgrounfdFetch (used for status polling etc)
        let settings = UIUserNotificationSettings(forTypes: UIUserNotificationType.Alert, categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        UIApplication.sharedApplication().setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalMinimum)
        
        
        return true
    }
    
    func isMultitaskingSupported() -> Bool{
        return UIDevice.currentDevice().multitaskingSupported
    }
    
    func timerMethod(sender: NSTimer){
        let timeRemaining = UIApplication.sharedApplication().backgroundTimeRemaining
        if timeRemaining == DBL_MAX {
            print("Time remaining = unknown")
        }
        else {
            print("Time remaining = \(timeRemaining) seconds")
        }
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
    }
    
    func application(application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: () -> Void) {
        
    }
    
    func application(application: UIApplication, performFetchWithCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        //need a global Docmail interface object to enable status polling & update os status in CoreData model
        
        /* get status for items that haven't reached and "end" state
                any errors (past retry limit)/validation fails should be badged/notified
            order created, card compiled, but not submitted         = submit in background
            order created, card not yet compiled                    = badge /notify of unfinished card with delete | resume
            order created, compiled, submitted, not yet complete    = update status
            "end states"
                cancelled
                deleted 
                complete
        
            process these orders in 
        */
        
        var username = ""
        var password = ""
        
        if KeychainService.loadUsername() != "" && KeychainService.loadPassword() != "" {
            username=KeychainService.loadUsername()
            password=KeychainService.loadPassword()
        }
        
        
        let docmail = DocmailMobileWrapper(forAccountWithUsername: username, password: password, wsResponseDelegate: self, callingApp: "Willysoft", useLive: true)
        docmail.checkBalance_Account()
        
    }
    
    
    func response(sourceName:String, data:Dictionary<String,AnyObject>){
        var success = true
        print(data)
        if let resultCode = data["Result"] as? String {
            if Int(resultCode) <= 0 {
                print(data["Msg"]!)
                success = false
            }
        }
        
        if !success {
            print(("Docmail didn't like that :(", message: data["Msg"] as! String))
            KeychainService.saveCredentials("", password: "")
        }
        else {        }
    }
    func progressUp(connection:NSURLConnection, bytesWritten: Int, totalBytesWritten : Int,totalBytesExpectedToWrite: Int){ }
    func error(data: NSData!, response: NSURLResponse!, error: NSError!) {
        dispatch_async(dispatch_get_main_queue()) {
            print(("Network error", message: "data: \(data)\nresponse: \(response)\nerror: \(error)"))
        }
    }
    func notConnected() {
        dispatch_async(dispatch_get_main_queue()) {
            print(("Network error", message: "not connected"))
        }
    }

    
    


    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        if !isMultitaskingSupported() { return }
        
        taskTimer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: "timerMethod:", userInfo: nil, repeats: true)
        
       // backgroundTaskId = application.beginBackgroundTaskWithExpirationHandler({ () -> Void in
       //     self!.endBackgroundTask()
       // })
        
        //in theory, things like screen state, part-filled forms etc. should be saved here, but they should also be saved on out of memory/ app will terminate (the latter probably covers all cases bar system crash/uncontrolled shutdown - then again, uncontrolled shutdown won't be caught by anything!)
        
        backgroundTaskId = application.beginBackgroundTaskWithName("timer task", expirationHandler: { () -> Void in
            self.endBackgroundTask()
        })
        

    }
    
    func endBackgroundTask(){
        //close timers etc. reset background task Id and declare an end to the task
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            if let timer = self.taskTimer{
                timer.invalidate()
                self.taskTimer = nil
                UIApplication.sharedApplication().endBackgroundTask(self.backgroundTaskId)
                self.backgroundTaskId = UIBackgroundTaskInvalid
            }
        }
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        
        if backgroundTaskId != UIBackgroundTaskInvalid {
            endBackgroundTask()
        }
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.placeholdername.Postcards_Demo" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1] as NSURL
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("Postcards_Demo", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("Postcards_Demo.sqlite")
        
        let mOptions = [NSMigratePersistentStoresAutomaticallyOption: true,            NSInferMappingModelAutomaticallyOption: true]
        
        var error: NSError? = nil
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: mOptions)
            
        } catch var error1 as NSError {
            coordinator = nil
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            dict[NSUnderlyingErrorKey] = error
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(error), \(error!.userInfo)")
            abort()
        } catch {
            fatalError()
        }
    
        return coordinator
    }()
    

//    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
//        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
//        // Create the coordinator and store
//        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
//        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("Postcards_Demo.sqlite")
//        
//        let mOptions = [NSMigratePersistentStoresAutomaticallyOption: true,            NSInferMappingModelAutomaticallyOption: true]
//        
//        var error: NSError? = nil
//        var failureReason = "There was an error creating or loading the application's saved data."
//        do {
//            try coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: mOptions)
//        } catch var error1 as NSError {
//            error = error1
//            coordinator = nil
//            // Report any error we got.
//            var dict = [String: AnyObject]()
//            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
//            dict[NSLocalizedFailureReasonErrorKey] = failureReason
//            dict[NSUnderlyingErrorKey] = error
//            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
//            // Replace this with code to handle the error appropriately.
//            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//            NSLog("Unresolved error \(error), \(error!.userInfo)")
//            abort()
//        } catch {
//            fatalError()
//        }
//        
//        return coordinator
//    }()
    
    

    lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if let moc = self.managedObjectContext {
            do {
                try moc.save()
            } catch {
                if moc.hasChanges  {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nsError = error as NSError
                    NSLog("Unresolved error \(nsError), \(nsError.userInfo)")
                    abort()
                }
            }
        }
    }
    

}

